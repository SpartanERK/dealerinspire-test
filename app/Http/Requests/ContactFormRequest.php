<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ContactFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function getRedirectUrl()
    {
        if (!$this->get('redirect')) {
            return parent::getRedirectUrl();
        }

        $url = $this->redirector->getUrlGenerator();

        return $url->to($this->get('redirect'));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'full_name' => 'required|max:255',
            'email' => 'required|email|max:255',
            'phone' => 'max:255',
            'message' => 'required'
        ];
    }
}
