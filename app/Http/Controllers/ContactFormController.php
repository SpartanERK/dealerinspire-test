<?php

namespace App\Http\Controllers;

use App\Http\Requests\ContactFormRequest;
use App\Services\ProcessContactForm;
use Illuminate\Http\Request;

class ContactFormController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ContactFormRequest $request)
    {
        app(ProcessContactForm::class)->execute($request->all());

        $request->session()->flash(
            'contact-form-submitted',
            'Your message has been sent to Guy Smiley!'
        );

        return redirect('/#contact');
    }
}
