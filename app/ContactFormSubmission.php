<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContactFormSubmission extends Model
{
    protected $fillable = ['full_name', 'email', 'phone', 'message'];
}
