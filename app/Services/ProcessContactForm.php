<?php

namespace App\Services;

use App\ContactFormSubmission;
use App\Mail\ContactFormSubmittedMail;
use Illuminate\Contracts\Mail\Mailer;

class ProcessContactForm
{
    const CONTACT_EMAIL = 'guy-smiley@example.com';

    protected $data;
    private $mailer;

    public function __construct(Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * Create and Persist the Contact Form Submission,
     * Then Queue it to be sent out as an Email
     * @param  array  $contactFormData
     * @return void
     */
    public function execute(array $contactFormData)
    {
        $submission = ContactFormSubmission::create($contactFormData);

        $this->mailer->to(self::CONTACT_EMAIL)
            ->queue(new ContactFormSubmittedMail($submission));
    }
}
