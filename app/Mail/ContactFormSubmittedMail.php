<?php

namespace App\Mail;

use App\ContactFormSubmission;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ContactFormSubmittedMail extends Mailable
{
    use Queueable, SerializesModels;

    public $submission;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(ContactFormSubmission $submission)
    {
        $this->submission = $submission;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from([
                'address' => config('mail.from.address'),
                'name' => $this->submission->full_name
            ])->replyTo($this->submission->email)
            ->subject($this->getSubject())
            ->markdown('emails.contact-form-submission');
    }

    protected function getSubject()
    {
        return "New contact form submission received from {$this->submission->full_name}";
    }
}
