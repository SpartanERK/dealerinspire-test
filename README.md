# Dealer Inspire PHP Code Challenge

Thank you for reviewing my entry in the Dealer Inspire PHP Code Challenge!

This entry is build with Laravel. A default ENV File has been included for easy testing. This ENV file connects to a sqlite database stored in `database/database.sqlite`, which has already been migrated.

## Using MySQL

If you would prefer to test using a local MySQL database, update the `DB_CONNECTION` in `.env` to `mysql` and update `DB_USERNAME` and `DB_PASSWORD` to match your database credentials. Then run the `create_database.sql` file to create the database we use, then run `php artisan migrate` to create any tables.

## Notes

* For easy installation, the `vendor` directory has been included in this repository, containing all composer dependencies.

* Outgoing emails will be logged to `storage/logs/laravel.log`

* Tests can be run with `vendor/bin/phpunit`.


Thanks for your consideration!

- Erik Jonasson
