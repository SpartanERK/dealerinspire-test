@component('mail::message')

Hey, Guy Smiley, someone wants to talk to you!

**Name:** {{ $submission->full_name  }}<br>
**Email Address:**  {{ $submission->email }}<br>
**Phone:** {{ $submission->phone ?? '_Not Provided_'}}

## Message
{{ $submission->message }}

@endcomponent
