@if (Session::get('contact-form-submitted'))
    <div class="row">
        <div class="col-xs-12 col-lg-10 col-lg-offset-1">
            <div class="alert alert-success alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{ Session::get('contact-form-submitted') }}
            </div>
        </div>
    </div>
@endif
@if ($errors->any())
    <div class="row">
        <div class="col-xs-12 col-lg-10 col-lg-offset-1">
            <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                @foreach ($errors->all() as $error)
                    {{ $error }} <br>
                @endforeach
            </div>
        </div>
    </div>
@endif
<form action="{{ route('contactForm.submit') }}" method="POST" id="contactForm">
    <div class="row">
        <div class="col-xs-12 col-lg-10 col-lg-offset-1">
            <div class="form-group">
                <label for="full_name">Full Name</label>
                <input class="form-control" type="text" name="full_name" value="{{ Request::old('full_name') }}" placeholder="Your name" required>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-lg-5 col-lg-offset-1">
            <div class="form-group">
                <label for="email">Email</label>
                <input class="form-control" type="text" name="email" value="{{ Request::old('email') }}" placeholder="Your email address..." required>
            </div>
        </div>
        <div class="col-xs-12 col-lg-5">
            <div class="form-group">
                <label for="phone">Phone</label>
                <input class="form-control" type="text" name="phone" value="{{ Request::old('phone') }}" placeholder="(Optional) Your phone number...">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-lg-10 col-lg-offset-1">
            <div class="form-group">
                <label for="message">Your Message</label>
                <textarea class="form-control" name="message" id="" cols="30" rows="10" placeholder="What's up?" required>{{ Request::old('message') }}</textarea>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-lg-10 col-lg-offset-1">
            <input type="submit" class="btn btn-default btn-block" value="Contact Guy Smiley">
        </div>
    </div>

    {{ csrf_field() }}
    <input type="hidden" name="redirect" value="/#contact">
</form>
