<?php

namespace Tests\Feature;

use App\ContactFormSubmission;
use App\Mail\ContactFormSubmittedMail;
use App\Services\ProcessContactForm;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Testing\Fakes\MailFake;
use Tests\TestCase;

class ProcessContactFormTest extends TestCase
{
    use DatabaseMigrations, DatabaseTransactions;

    /**
     * @test
     * @group contact-form
     */
    function contact_form_mail_is_sent()
    {
        $mailer = new MailFake;
        $service = new ProcessContactForm($mailer);
        $testData = [
            'full_name' => $this->faker->name,
            'email' => $this->faker->safeEmail,
            'phone' => '',
            'message' => $this->faker->text(200),
            '_token' => csrf_token()
        ];


        $service->execute($testData);

        $mailer->assertQueued(ContactFormSubmittedMail::class, function ($mail) {
            return $mail->hasTo('guy-smiley@example.com');
        });
    }

    /**
     * @test
     * @group contact-form
     */
    function contact_form_submission_is_saved()
    {
        $mailer = new MailFake;
        $service = new ProcessContactForm($mailer);

        $testData = [
            'full_name' => $this->faker->name,
            'email' => $this->faker->safeEmail,
            'phone' => '',
            'message' => $this->faker->text(200),
            '_token' => csrf_token()
        ];

        $service->execute($testData);

        $submission = ContactFormSubmission::where('full_name', $testData['full_name'])
            ->where('email', $testData['email']);

        $this->assertNotNull($submission);
    }
}
