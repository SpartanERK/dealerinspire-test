<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ContactFormValidationTest extends TestCase
{
    use DatabaseMigrations, DatabaseTransactions;

    /**
     * @test
     * @group contact-form
     */
    function contact_form_submission_rejects_requests_missing_full_name()
    {
        $response = $this->post('/forms/contact', [
            'full_name' => '',
            'email' => $this->faker->safeEmail,
            'phone' => $this->faker->phoneNumber,
            'message' => $this->faker->text(200),
            '_token' => csrf_token()
        ]);

        $response->assertStatus(302);
        $this->assertArrayHasKey('full_name', $response->exception->errors());
    }

    /**
     * @test
     * @group contact-form
     */
    function test_contact_form_submission_rejects_requests_missing_email()
    {
        $response = $this->post('/forms/contact', [
            'full_name' => $this->faker->name,
            'email' => '',
            'phone' => $this->faker->phoneNumber,
            'message' => $this->faker->text(200),
            '_token' => csrf_token()
        ]);

        $response->assertStatus(302);
        $this->assertArrayHasKey('email', $response->exception->errors());
    }

    /**
     * @test
     * @group contact-form
     */
    function contact_form_submission_accepts_requests_missing_phone()
    {
        $response = $this->post('/forms/contact', [
            'full_name' => $this->faker->name,
            'email' => $this->faker->safeEmail,
            'phone' => '',
            'message' => $this->faker->text(200),
            '_token' => csrf_token()
        ]);

        $this->assertEmpty($response->exception);
    }

    /**
     * @test
     * @group contact-form
     */
    function failed_validation_uses_redirect_value_if_provided()
    {
        $response = $this->post('/forms/contact', [
            'full_name' => '',
            'email' => $this->faker->safeEmail,
            'phone' => $this->faker->phoneNumber,
            'message' => $this->faker->text(200),
            '_token' => csrf_token(),
            'redirect' => '/foo'
        ]);

        $response->assertStatus(302);
        $this->assertEquals(env('APP_URL') . 'foo', $response->getTargetUrl());
    }
}
