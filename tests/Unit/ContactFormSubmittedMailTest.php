<?php

namespace Tests\Unit;

use App\ContactFormSubmission;
use App\Mail\ContactFormSubmittedMail;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Testing\Fakes\MailFake;
use Tests\TestCase;

class ContactFormSubmittedMailTest extends TestCase
{

    /**
     * @test
     * @group contact-form
     */
    function message_populates_submission_data()
    {
        $mailer = new MailFake;
        $submission = factory(ContactFormSubmission::class)->make();
        $mailer->to('test@example.com')->send(new ContactFormSubmittedMail($submission));

        $mailer->assertSent(ContactFormSubmittedMail::class, function ($mail) use ($submission) {
            $mail->build();
            $this->assertEquals(
                "New contact form submission received from {$submission->full_name}",
                $mail->subject
            );

            return $mail->hasReplyTo($submission->email);
        });
    }
}
